package com.example.soundrecorder;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    ToggleButton btnRecord;
    String pathSave = Environment.getExternalStorageDirectory()+ "/SoundRecords/" +android.text.format.DateFormat.format("yyyy-MM-dd_HH-mm-ss", new java.util.Date())+"_aufnahme.3gp";
    MediaRecorder mediaRecorder;
    public TextView timerTextView;
    private long startHTime = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    final int REQUEST_PERMISSION_CODE = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createDirectory();

        //Button für den Start der Aufnahme und Textfield für den Timer
        btnRecord = findViewById(R.id.btnRecord);
        timerTextView = findViewById(R.id.textView2);

        //Überprüfen der Berechtigungen
        if(!checkPermissionFromDevice()) {
            requestPermission();
        }
    }

    public void createDirectory(){
        File folder = new File(Environment.getExternalStorageDirectory()+"/SoundRecords");

        if(!folder.exists()) {
            if(folder.mkdir());
        }
    }

    //Methode um zu schauen ob der Togglebutton aktiv ist oder nicht
    public void toggleButtonAction(View view){
        if (btnRecord.isChecked()){
            startRecording();
        }
        else if(!btnRecord.isChecked()){
            stopRecording();
        }
    }

    private void startRecording(){
        setupMediaRecorder();

        try{
            //MediaRecorder macht sich bereit und startet anschliessend, zudem wird der Timer gestartet
            mediaRecorder.prepare();
            startHTime = SystemClock.uptimeMillis();
            customHandler.postDelayed(updateTimerThread, 0);
            mediaRecorder.start();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        Toast.makeText(MainActivity.this, "Aufnahme...", Toast.LENGTH_SHORT).show();
    }

    private void stopRecording(){
        //MediaRecorder stoppt, setzt sich zurück und wird wieder "entfernt"
        mediaRecorder.stop();
        mediaRecorder.reset();
        mediaRecorder.release();
        resetTimer(); //Methode um den Timer zurückzusetzen
        Toast.makeText(MainActivity.this, "Aufnahme beendet.", Toast.LENGTH_SHORT).show();
        Log.d("LOG", "Aufnahme unter " + pathSave + " gespeichert.");
    }

    //Allgemeine Einstellungen für den MediaRecorder
    private void setupMediaRecorder(){
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(pathSave);
    }

    //Pop-Up beim starten der App die nach Berechtigungen fragt
    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO
        }, REQUEST_PERMISSION_CODE);
    }

    //Zeigt das Ergebnis der vorher angefragten Berechtigungen an
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode)
        {
            case REQUEST_PERMISSION_CODE:
            {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this, "Berechtigung bekommen", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "Berechtigung verwährt", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //Berechtigungen für die Hardware überprüfen (Mikrofon & Speicher)
    private boolean checkPermissionFromDevice() {
        int write_external_storage_result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int record_audio_result = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);

        return write_external_storage_result == PackageManager.PERMISSION_GRANTED &&
                record_audio_result == PackageManager.PERMISSION_GRANTED;
    }

    //Methode für den Timer der während der Aufnahme läuft
    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startHTime;

            updatedTime = timeSwapBuff + timeInMilliseconds;

            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            if (timerTextView != null)
                timerTextView.setText(String.format("%s:%s", String.format("%02d", mins), String.format("%02d", secs)));
            customHandler.postDelayed(this, 0);
        }
    };

    //Methode um Timer wieder zurückzusetzen
    public void resetTimer(){
        timeSwapBuff += timeInMilliseconds;
        customHandler.removeCallbacks(updateTimerThread);

        startHTime = 0L;
        timeInMilliseconds = 0L;
        timeSwapBuff = 0L;
        updatedTime = 0L;
        timerTextView.setText("00:00");
    }

    //Activitynavigation
    public void savedActivity(View view){
        Intent intent = new Intent(MainActivity.this, SavedActivity.class);
        startActivity(intent);
    }
}
