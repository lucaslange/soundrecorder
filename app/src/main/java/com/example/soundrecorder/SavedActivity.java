package com.example.soundrecorder;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TableLayout;
import java.io.File;

public class SavedActivity extends AppCompatActivity {

    MediaPlayer mediaPlayer = new MediaPlayer();
    SeekBar seekBar;
    Button playBtn;
    int fileLength;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved);

        //Einlesen der Audiofiles
        TableLayout table = findViewById(R.id.btnTable);
        String path = Environment.getExternalStorageDirectory()+ "/SoundRecords/" +android.text.format.DateFormat.format("yyyy-MM-dd_HH-mm-ss", new java.util.Date())+"_aufnahme.3gp";
        File directory = new File(Environment.getExternalStorageDirectory()+"/SoundRecords");
        File[] files = directory.listFiles();

        Log.d("Files", "Anzahl: "+ files.length);

        for (int i = 0; i < files.length; i++)
        {
            final String name = files[i].getName();
            final String param = files[i].toString();

            Log.d("Files", "Filename:" + files[i].getName());
            //Audiofile wird als Button angezeigt (Aufnahmen Reiter)
            Button button = new Button(this.getApplicationContext());
            button.setText(name);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createDialog(param);
                }
            });
            table.addView(button);
        }
    }

    //Methode um den Dialog zu erstellen beim Klick auf ein Audiofile
    private void createDialog(final String param){

        final Dialog dialog = new Dialog(SavedActivity.this);
        dialog.setContentView(R.layout.activity_popup);
        dialog.setTitle("Musikplayer");

        playBtn = dialog.findViewById(R.id.play);
        seekBar = dialog.findViewById(R.id.seekBar);

        seekBar.setProgress(0);
        seekBar.setMax(100);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mediaPlayer.pause();
            }
        });

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                seekBar.setProgress(0);

                try{
                    if(mediaPlayer.isPlaying()){
                        mediaPlayer.reset();
                    }
                    mediaPlayer.reset();
                    mediaPlayer.setDataSource(param);
                    mediaPlayer.prepare();
                    mediaPlayer.setVolume(1f, 1f);
                    mediaPlayer.setLooping(false);
                    mediaPlayer.start();
                    seekBarUpdater();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                fileLength = mediaPlayer.getDuration();
            }
        });
        dialog.show();
    }


    //Sorgt dafür, dass die SeekBar mit dem Audiofile läuft
    private void seekBarUpdater() {
        seekBar.setProgress((int)(((float)mediaPlayer.getCurrentPosition()/ fileLength) *100));
        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run() {
                    seekBarUpdater();
                }
            };
            handler.postDelayed(notification,1000);
        }
    }

    //Activitynavigation
    public void mainActivity(View view){
        Intent intent = new Intent(SavedActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
